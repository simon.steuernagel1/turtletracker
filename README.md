# turtletracker

This repository contains a ROS2 package implementing a custom multiple extended object tracker. Code is entirely python
based, and all relevant algorithms are implemented based on numpy, i.e., ROS data formats are used as a front end for
communication.

## Using this package
After installing the package to `colcon_ws/src`, build it using 
```
colcon build
```
and remember to source your setup.bash by doing 
```
source install/setup.bash
```

Afterwards, you can run tracking by calling
```
ros2 run turtletracker tracking
```

## Repository Structure Overview
- [./turtletracker](./turtletracker) Contains all ROS node implementations as well as subdirectories for modules used
by the nodes
- [./turtletracker/src](./turtletracker/src) Contains utility source code used by the nodes
  - [./turtletracker/src/mot](./turtletracker/src/mot) Contains Multi Object Tracking implementations (without ros 
  dependencies)
  - [./turtletracker/src/eot](./turtletracker/src/eot) Contains Single Extended Object Tracking implementations (without
  ros dependencies)
  - [./turtletracker/src/ros-base](./turtletracker/src/ros_base) Contains ROS-dependent wrapper code such as conversion 
  to and from ROS data types
  - [./turtletracker/src/utils](./turtletracker/src/utils) Contains all further utility functions (without ros 
  dependencies)
- [./output](./output) Contains all output produced by the programs such as graphics
- [./doc](./doc) Contains additional documentation
  - [./doc/visuals](./doc/visuals) Contains static visuals such as figures that are used within `.md` files etc.
- [./test](./test) Contains python tests to run (ROS2 package standard)
- [./resource](./resource) Contains resources (ROS2 package standard)

## Useful Links

- [TB3 Wiki (slam page)](https://emanual.robotis.com/docs/en/platform/turtlebot3/slam/#run-slam-node)
- [Turtlebot Bringup wiki](http://wiki.ros.org/turtlebot3_bringup) - **Topic List!**
- [Multi-Robot Environment Setup Guide](https://discourse.ros.org/t/giving-a-turtlebot3-a-namespace-for-multi-robot-experiments/10756/6)
