import numpy as np


def rot(alpha: float):
    """
    Creates a rotation matrix.
    :param alpha:   rotation in radian
    :return:        2x2 rotation matrix corresponding to alpha
    """
    return np.array([
        [np.cos(alpha), -np.sin(alpha)],
        [np.sin(alpha),  np.cos(alpha)],
    ])


def process_covariance_cartesian(time_difference: float, sigma_v: np.ndarray):
    """
    Calculates the process noise covariance for a Cartesian velocity nearly constant velocity model with assumed
    discrete acceleration noise.
    Yaakov Bar-Shalom, X. Rong Li, and Thiagalingam Kirubarajan.Estimation with Applications to Tracking and Navigation:
    Theory Algorithms and Software. John Wiley &Sons, 2004
    :param time_difference: time difference over which process noise is added
    :param sigma_v:         2D array containing standard deviations of vx and vy
    :return:                4x4 process noise covariance matrix (x, y, vx, vy)
    """
    sigma_v = np.array(sigma_v)
    G = np.array([
        [0.5 * time_difference**2,                      0.0],
        [0.0,                      0.5 * time_difference**2],
        [time_difference,                               0.0],
        [0.0,                               time_difference],
    ])
    return G @ np.diag(sigma_v**2) @ G.T


def ellipse_parameters_from_matrix(shape_matrix: np.ndarray):
    """
    Calculates orientation and semi-axes lengths from the input shape matrix.
    :param shape_matrix:    2D SPD shape matrix
    :return:                orientation in radian (-PI to PI) and semi-axis lengths with the first semi-axis
                            corresponding to the orientation
    """
    eigenvalues, eigenvectors = np.linalg.eig(shape_matrix)
    alpha = ((np.arctan2(eigenvectors[1, 0], eigenvectors[0, 0]) + np.pi) % (2*np.pi)) - np.pi
    return alpha, np.sqrt(eigenvalues[0]), np.sqrt(eigenvalues[1])
