import numpy as np
import json

from turtletracker.src.mot.simple_dbscan_partitioner import SimpleDBSCANPartitioner
from turtletracker.src.mot.pointwise_gnn_associator import PointwiseGNNAssociator
from turtletracker.src.eot.rm_tracker import RMTracker

PARTITIONER_REFERENCES = {
    "simple_dbscan": SimpleDBSCANPartitioner
}

ASSOCIATOR_REFERENCES = {
    "pointwise_gnn": PointwiseGNNAssociator
}

TRACKER_REFERENCES = {
    "random_matrix": RMTracker
}


def load_baseline_mot_config(path_to_config: str) -> dict:
    """
    Load a json config for a baseline_mot object, parse necessary fields, and return the result
    :param path_to_config: String file path to json to load
    :return:
    """
    with open(path_to_config) as f:
        data: dict = json.load(f)

    # replace necessary str keys with class references
    data["partitioner"] = PARTITIONER_REFERENCES[data["partitioner"]](**data["partitioner_parameters"])
    data["associator"] = ASSOCIATOR_REFERENCES[data["associator"]](**data["associator_parameters"])
    data["tracker_type"] = TRACKER_REFERENCES[data["tracker_type"]]

    data.pop("partitioner_parameters")
    data.pop("associator_parameters")

    return data



if __name__ == '__main__':
    load_baseline_mot_config("../../../configs/baseline_mot/default.json")
