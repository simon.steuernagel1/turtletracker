import numpy as np

from scipy.linalg import sqrtm

from turtletracker.src.utils.support_functions import process_covariance_cartesian, ellipse_parameters_from_matrix
from turtletracker.src.eot.abstract_eot import AbstractEllipticalObjectTracker


class RMTracker(AbstractEllipticalObjectTracker):
    """
    Random matrix tracker, see, e.g.,
    Granström, Karl; Baum, Marcus (2022): A Tutorial on Multiple Extended Object Tracking. TechRxiv.
    Preprint. https://doi.org/10.36227/techrxiv.19115858.v1
    Expects birth parameters
    init_cov:   initial kinematic state covariance as a 4x4 SPD matrix
    init_v:     initial scale parameter for inverse Wishart density with tau > 6
    sigma_v:    2D process noise standard deviation on vx and vy
    tau:        time update parameter for inverse Wishart state
    R:          2x2 measurement noise covariance (x, y)
    """
    def __init__(self,
                 measurements: np.ndarray,
                 birth_parameters: {}):
        """
        Create new instance at the provided measurement cluster (M-D array with (x,y,intensity) using provided birth
        parameters
        """
        super().__init__(measurements, birth_parameters)

        self._scale = 0.25  # scaling parameter for measurement distribution across shape
        self._sigma_v = birth_parameters.get('sigma_v')
        self._tau = birth_parameters.get('tau')
        self._R = np.array(birth_parameters.get('R'))

        # kinematic state as Gaussian random vector (x, y, vx, vy)
        self._kin_state = np.zeros(4)
        self._kin_cov = np.array(birth_parameters.get('init_cov'))

        # shape state as inverse Wishart random matrix
        self._V = np.zeros((2, 2))
        self._v = birth_parameters.get('init_v')

        # initialize state
        self._kin_state[:2] = np.mean(measurements[:, :2], axis=0)
        if len(measurements) > 1:
            self._V = np.mean(np.einsum('xa, xb -> xab', measurements[:, :2] - self._kin_state[:2],
                                        measurements[:, :2] - self._kin_state[:2]), axis=0) \
                      * (self._tau - 6.0) / self._scale
        else:
            self._V = np.eye(2) * 1.0

    def predict(self, time_difference: float):
        """Predict forward in time"""
        F = np.array([
            [1, 0, time_difference,               0],
            [0, 1, 0,               time_difference],
            [0, 0, 1,                             0],
            [0, 0, 0,                             1],
        ])

        # kinematics_
        self._kin_state = F @ self._kin_state
        self._kin_cov = F @ self._kin_cov @ F.T + process_covariance_cartesian(time_difference, self._sigma_v)

        # shape:
        # decay v_minus by e(-T/tau)
        # to prevent v_plus from being too small: only decay a portion greater than 2*d-2
        v_plus = np.exp(-time_difference / self._tau) * (self._v - 6.0) + 6.0
        self._V = ((v_plus - 3.0) / (self._v - 3.0)) * self._V
        self._v = v_plus

    def update(self, measurements: np.ndarray):
        """Update with N Measurements, given as list of shape (N,3) (x-y-intensity)"""
        measurements = measurements[:, :2]
        H = np.array([
            [1, 0, 0, 0],
            [0, 1, 0, 0],
        ])
        number_measurements = len(measurements)
        z_bar = np.mean(measurements, axis=0)
        innovation = z_bar - H @ self._kin_state

        spread = np.einsum('xa, xb -> ab', measurements - z_bar[None, :], measurements - z_bar[None, :])

        X_hat = self._V / (self._v - 6.0)
        Y = self._scale * X_hat + self._R
        S = H @ self._kin_cov @ H.T + Y / number_measurements
        S_inv = np.linalg.inv(S)
        K = self._kin_cov @ H.T @ S_inv

        X_2: np.ndarray = sqrtm(X_hat)
        S_i2: np.ndarray = sqrtm(S_inv)
        Y_i2: np.ndarray = sqrtm(np.linalg.inv(Y))

        innovation = innovation.reshape((-1, 1))
        N_hat = X_2 @ S_i2 @ innovation @ innovation.T @ S_i2.T @ X_2.T
        Z_hat = X_2 @ Y_i2 @ spread @ Y_i2.T @ X_2.T

        self._kin_state = self._kin_state + (K @ innovation).reshape(self._kin_state.shape)
        self._kin_cov = self._kin_cov - K @ S @ K.T
        self._v = self._v + number_measurements
        self._V = self._V + N_hat + Z_hat
        self._V = 0.5 * (self._V + self._V.T)
        self._V = np.real(self._V)

    def get_state(self):
        """
        Return the current state of the objects as a list of (state, covariance).
        state: 7D: x, y, vx, vy, orientation (rad), semi-axis length, semi-axis width
        cov: 7x7 np.ndarray - covariance of state
        """
        large_cov = np.zeros((7, 7))
        large_cov[:4, :4] = self._kin_cov.copy()
        # state = np.hstack([self._kin_state.copy(), ellipse_parameters_from_matrix(self._V / (self._v - 6.0))])
        state = np.zeros((7,))
        state[:4] = self._kin_state
        state[4:] = ellipse_parameters_from_matrix(self._V / (self._v - 6.0))
        return state, large_cov
