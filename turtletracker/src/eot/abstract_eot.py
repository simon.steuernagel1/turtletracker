from abc import ABC, abstractmethod

import numpy as np


class AbstractObjectTracker(ABC):
    """
    Abstract base class for object trackers, needs to be implemented by more specific abstract classes for
    object trackers
    """
    @abstractmethod
    def __init__(self, measurements: np.ndarray, birth_parameters: {}):
        """
        Create new instance at the provided measurement cluster (M-D array with (x,y,intensity) using provided initial
        birth parameters.
        """
        pass

    @abstractmethod
    def predict(self, time_difference: float):
        """Predict forward in time"""
        pass

    @abstractmethod
    def update(self, measurements: np.ndarray):
        """Update with N Measurements, given as list of shape (N,3) (x-y-intensity)"""
        pass

    @abstractmethod
    def get_state(self):
        """
        Return the current state of the objects as a list of (state, covariance).
        state: not further specified stateND np.ndarray
        cov: NxN np.ndarray - covariance of state
        """


class AbstractEllipticalObjectTracker(AbstractObjectTracker):
    """Abstract base class for extended object trackers"""
    @abstractmethod
    def __init__(self, measurements: np.ndarray, birth_parameters: {}):
        """
        Create new instance at the provided measurement cluster (M-D array with (x,y,intensity) using provided initial
        birth parameters.
        """
        super().__init__(measurements, birth_parameters)
        pass

    @abstractmethod
    def predict(self, time_difference: float):
        """Predict forward in time"""
        pass

    @abstractmethod
    def update(self, measurements: np.ndarray):
        """Update with N Measurements, given as list of shape (N,3) (x-y-intensity)"""
        pass

    @abstractmethod
    def get_state(self):
        """
        Return the current state of the objects as a list of (state, covariance).
        state: 7D: x, y, vx, vy, orientation (rad), semi-axis length, semi-axis width
        cov: 7x7 np.ndarray - covariance of state
        """
