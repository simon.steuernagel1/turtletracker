import numpy as np
from scipy.spatial.distance import cdist
from scipy.optimize import linear_sum_assignment

from turtletracker.src.mot.abstract_associator import AbstractAssociator
from turtletracker.src.eot.abstract_eot import AbstractObjectTracker


class PointwiseGNNAssociator(AbstractAssociator):
    def __init__(self, gating_distance, distance_metric='euclidean'):
        """
        Create a new PointwiseGNNAssociator object.
        :param gating_distance: Maximum distance between a cluster and object mean for the two to be associated
        """
        self.gating_distance = gating_distance
        self._distance_metric = distance_metric

        assert self._distance_metric in ['euclidean', 'mahalanobis']

    def associate(self, objects: {int: AbstractObjectTracker}, measurements: np.ndarray) -> {}:
        """
        Run the Hungarian algorithm on the center-points of objects and clusters and return the resulting mapping.
        :param objects: Dict mapping object id to trackers
        :param measurements: Nx4 array of measurements
        :return:
        """
        if len(objects) == 0 or len(measurements) == 0:
            return {}
        # compute object/cluster means and save mappings from array index to "true" id for later
        object_id_mapping, object_centers, covariances = self.get_centers_from_objects(objects)
        cluster_id_mapping, cluster_centers = self.get_centers_from_clusters(measurements)

        # calculate distance and solve assignment problem using Hungarian algorithm
        if self._distance_metric == 'mahalanobis':
            distance_matrix = cdist(object_centers, cluster_centers, metric=self._distance_metric,
                                    VI=np.linalg.inv(covariances))
        else:
            distance_matrix = cdist(object_centers, cluster_centers, metric=self._distance_metric)
        row_ind, col_ind = linear_sum_assignment(distance_matrix)

        resulting_mapping = {}
        for r_ix, c_ix in zip(row_ind, col_ind):
            # check for gating distance
            if distance_matrix[r_ix, c_ix] >= self.gating_distance:
                # distance is larger than whats allowed, ignore this match
                continue

            # convert row/col index to actual object/cluster index using previously acquired mappings
            object_id = object_id_mapping[r_ix]
            cluster_id = cluster_id_mapping[c_ix]
            # create mapping of object_id to cluster_id
            resulting_mapping[object_id] = cluster_id

        # return result
        return resulting_mapping

    @staticmethod
    def get_centers_from_objects(objects: {int: AbstractObjectTracker}):
        id_list = []
        means = []
        covariances = []

        for obj_id in objects.keys():
            id_list.append(obj_id)
            means.append(objects[obj_id].get_state()[0][:2])
            covariances.append(objects[obj_id].get_state()[1][:2, :2])
        id_list = np.array(id_list)
        means = np.array(means).reshape((-1, 2)).astype(float)
        covariances = np.array(covariances).reshape((-1, 2, 2)).astype(float)
        return id_list, means, covariances

    @staticmethod
    def get_centers_from_clusters(measurements: np.ndarray):
        id_list = []
        means = []

        for cluster_id in np.unique(measurements[:, 3]):
            id_list.append(cluster_id)
            # filter to list of points corresponding to current id
            cluster_points = measurements[measurements[:, 3] == cluster_id]
            # reduce to x-y component and average
            means.append(np.average(cluster_points[:, :2], axis=0))

        id_list = np.array(id_list)
        means = np.array(means).reshape((-1, 2)).astype(float)
        return id_list, means
