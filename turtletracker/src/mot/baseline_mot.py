import numpy as np

import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse

from turtletracker.src.mot.abstract_mot import AbstractMultiObjectTracker
from turtletracker.src.eot.abstract_eot import AbstractObjectTracker
from turtletracker.src.mot.abstract_partitioner import AbstractPartitioner
from turtletracker.src.mot.abstract_associator import AbstractAssociator


class BaselineMOT(AbstractMultiObjectTracker):
    """
    Baseline tracker keeping list of objects, predicts them to current time step and updates with current measurement
    point cloud. The point cloud is partitioned and each partition is associated to the objects in a 1-1 manner.
    Partitions which are not associated to an object are used to initialize new objects.
    """

    def __init__(self,
                 birth_parameters: {},
                 partitioner: AbstractPartitioner,
                 associator: AbstractAssociator,
                 tracker_type,
                 start_time: float = 0.0,
                 min_measurements_to_confirm: int = 3,
                 max_misses_to_delete_confirmed: int = 3,
                 max_misses_to_delete_unconfirmed: int = 1,
                 min_velocity_to_confirm: float = 0.1,
                 min_velocity_to_stay_confirmed: float = 0.05,
                 ):
        """
        Initialize tracker.
        :param birth_parameters:                    parameters given to newly initialized objects
        :param partitioner:                         creates measurement partitions
        :param associator:                          associates objects to measurement clusters
        :param tracker_type:                         reference to an AbstractObjectTrackerClass used to initialize new
                                                    objects
        :param start_time:                          initial time, default 0
        :param min_measurements_to_confirm:         new object requires this many detections in a row to be confirmed
        :param max_misses_to_delete_confirmed:      delete a confirmed object if it is missed this number of times
        :param max_misses_to_delete_unconfirmed:    delete an unconfirmed object if it is missed this number of times
        """
        self._birth_parameters = birth_parameters
        self._partitioner = partitioner
        self._associator = associator
        self._object_type = tracker_type
        self._current_time = start_time
        self._min_measurements_to_confirm = min_measurements_to_confirm
        self._max_misses_to_delete_confirmed = max_misses_to_delete_confirmed
        self._max_misses_to_delete_unconfirmed = max_misses_to_delete_unconfirmed
        self._min_velocity_to_confirm = min_velocity_to_confirm
        self._min_velocity_to_stay_confirmed = min_velocity_to_stay_confirmed

        self._objects: {int: AbstractObjectTracker} = {}
        self._measurements_and_misses_in_a_row: {int: int} = {}  # for each object, positive is detect, negative is miss
        self._confirmed_ids = []
        self._partitioned_measurements = np.zeros((0, 4))
        self._filtered_partitioned_measurements = np.zeros((0, 4))
        self._new_id = 0  # counter for new unique IDs

    def predict(self, time_difference: float):
        """
        Predict forward in time
        :param time_difference: time difference to predict
        """
        for key in self._objects:
            self._objects[key].predict(time_difference)

    def update(self, measurements: np.ndarray):
        """
        Update with N Measurements, given as list of shape (N,3) (x-y-intensity)
        :param measurements: measurement point cloud of the current time step, Nx3: x, y, intensity
        """
        # create measurement partitions
        self._partitioned_measurements = self._partitioner.create_partitions(measurements)
        self._filtered_partitioned_measurements = self._partitioner.filter_partitions(self._partitioned_measurements)

        # associate partitions to objects in 1-1 manner
        association = self._associator.associate(self._objects, self._filtered_partitioned_measurements)

        for key in association:
            associated_ids = self._partitioned_measurements[:, 3] == association[key]
            # update object with associated measurements
            self._objects[key].update(self._partitioned_measurements[associated_ids, :3])
            if self._measurements_and_misses_in_a_row[key] < 0:
                self._measurements_and_misses_in_a_row[key] = 1
            else:
                self._measurements_and_misses_in_a_row[key] += 1  # TODO: cap counting up

        # keep track of missed objects
        for key in self._objects:
            if key not in association.keys():
                if self._measurements_and_misses_in_a_row[key] > 0:
                    self._measurements_and_misses_in_a_row[key] = -1
                else:
                    self._measurements_and_misses_in_a_row[key] -= 1  # TODO: cap counting down

        # initialize a new object for unassociated measurement cluster
        for cluster_id in np.unique(self._filtered_partitioned_measurements[:, 3]):
            if cluster_id not in association.values():
                associated_ids = self._partitioned_measurements[:, 3] == cluster_id
                self._objects[self._new_id] \
                    = self._object_type(self._partitioned_measurements[associated_ids, :3], self._birth_parameters)
                self._measurements_and_misses_in_a_row[self._new_id] = 1
                self._new_id += 1

        # confirm birth targets and pruning
        remove_keys = []
        for key in self._objects.keys():
            if key not in self._confirmed_ids:
                # confirm or remove birth objects
                if (self._measurements_and_misses_in_a_row[key] >= self._min_measurements_to_confirm) \
                        & (np.linalg.norm(self._objects[key].get_state()[0][2:4]) > self._min_velocity_to_confirm):
                    self._confirmed_ids.append(key)
                elif self._measurements_and_misses_in_a_row[key]*-1 >= self._max_misses_to_delete_unconfirmed:
                    remove_keys.append(key)
            else:
                # check whether confirmed objects need to be pruned
                if self._measurements_and_misses_in_a_row[key]*-1 >= self._max_misses_to_delete_confirmed:
                    remove_keys.append(key)
                elif np.linalg.norm(self._objects[key].get_state()[0][2:4]) < self._min_velocity_to_stay_confirmed:
                    self._confirmed_ids.remove(key)

        for key in remove_keys:
            self.remove_object(key, confirmed=(key in self._confirmed_ids))

    def remove_object(self, key: int, confirmed: bool = True):
        """
        Remove an object from the tracker.
        :param key:         ID of the object
        :param confirmed:   bool flagging whether object has already been confirmed or not
        :return:
        """
        self._objects.pop(key)
        self._measurements_and_misses_in_a_row.pop(key)
        if confirmed:
            self._confirmed_ids.remove(key)

    def get_confirmed_objects(self):
        """
        Returns confirmed objects.
        :return: Return the current confirmed objects as a list of (UID, state, covariance) tuples.
                    UID:    int - unique object id
                    state:  7D: x, y, vx, vy, orientation (rad), length, width
                    cov:    7x7 np.ndarray - covariance of state
        """
        return [(key, *self._objects[key].get_state()) for key in self._objects if key in self._confirmed_ids]

    def get_unconfirmed_objects(self):
        """
        Returns newly birthed but not yet confirmed objects.
        :return: Return the current unconfirmed  (but internally tracked) objects as a list of (UID, state, covariance).
                    UID:    int - unique object id
                    state:  7D: x, y, vx, vy, orientation (rad), length, width
                    cov:    7x7 np.ndarray - covariance of state
        """
        return [(key, *self._objects[key].get_state()) for key in self._objects if key not in self._confirmed_ids]

    def get_last_partition(self):
        """
        Return last measurement partition.
        :return: last measurement partition as np.ndarray, Nx4 (x, y, intensity, cluster_ID) with clutter marked with
                 ID -1
        """
        return self._partitioned_measurements

    def get_last_filtered_partition(self):
        """
        Return last measurement partition after preprocessing potentially removed some clusters.
        :return: last measurement partition as np.ndarray, Nx4 (x, y, intensity, cluster_ID) with unwanted measurements
        from clutter or filtered clusters removed
        """
        return self._filtered_partitioned_measurements
