from abc import ABC, abstractmethod

import numpy as np

from turtletracker.src.eot.abstract_eot import AbstractObjectTracker


class AbstractAssociator(ABC):
    """Abstract base class for partitioning of point clouds"""
    @abstractmethod
    def associate(self, objects: {int: AbstractObjectTracker}, measurements: np.ndarray) -> {}:
        """
        Associates objects (dictionaries of AbstractObjectTrackers) to measurement partitions (measurements as
        Nx4 ndarray (x, y, intensity, cluster_ID), an ID of -1 is ignored as clutter.)
        Returns dictionary associating object IDs from the input dictionary to the cluster_IDs of the measurements.
        Unassociated clusters and objects are ignored. Returns empty dictionary if nothing is associated.
        """
