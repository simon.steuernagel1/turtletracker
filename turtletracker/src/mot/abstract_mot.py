from abc import ABC, abstractmethod

import numpy as np


class AbstractMultiObjectTracker(ABC):
    """Abstract base class that should be implemented by Trackers"""
    @abstractmethod
    def predict(self, time_difference: float):
        """Predict forward in time"""
        pass

    @abstractmethod
    def update(self, measurements: np.ndarray):
        """Update with N Measurements, given as list of shape (N,3) (x-y-intensity)"""
        pass

    @abstractmethod
    def get_confirmed_objects(self):
        """
        Return the current confirmed objects as a list of (UID, state, covariance).
        UID: int - unique object id
        state: 7D: x, y, vx, vy, orientation (rad), length, width
        cov: 7x7 np.ndarray - covariance of state
        """
        pass

    @abstractmethod
    def get_unconfirmed_objects(self):
        """
        Return the current unconfirmed  (but internally tracked) objects as a list of (UID, state, covariance).
        UID: int - unique object id
        state: 7D: x, y, vx, vy, orientation (rad), length, width
        cov: 7x7 np.ndarray - covariance of state
        """
        pass

    @abstractmethod
    def get_last_partition(self):
        """
        Return last measurement partition as np.ndarray, Nx4 (x, y, intensity, cluster_ID) with clutter marked with
        ID -1.
        """
        pass

    @abstractmethod
    def get_last_filtered_partition(self):
        """
        Return last measurement partition after preprocessing potentially removed some clusters as np.ndarray,
        Nx4 (x, y, intensity, cluster_ID) with clutter marked with ID -1.
        """
        pass
