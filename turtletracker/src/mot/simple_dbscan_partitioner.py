"""
Implements a simple baseline Partitioner based on the DBSCAN algorithm
"""
import numpy as np
from sklearn.cluster import DBSCAN
from scipy.spatial.distance import cdist

from turtletracker.src.mot.abstract_partitioner import AbstractPartitioner


class SimpleDBSCANPartitioner(AbstractPartitioner):
    def __init__(self,
                 eps,
                 min_samples,
                 max_samples,
                 max_object_size,
                 min_pts_line,
                 min_length_line,
                 wall_precision):
        self.eps = eps
        self.min_samples = min_samples
        self.max_samples = max_samples
        self.max_object_size = max_object_size
        self.min_pts_line = min_pts_line
        self.min_length_line = min_length_line
        self.wall_precision = wall_precision

    def create_partitions(self, measurements: np.ndarray) -> np.ndarray:
        """
        Run DBSCAN on the given measurements and return the measurements with labels appended to it
        :param measurements: Nx3 measurement array
        :return: Nx4 measurements, labels array
        """
        # TODO handle empty measurements
        wall_indices, non_wall_indices = self.filter_walls(measurements)
        cluster_labels = np.zeros(len(measurements))
        if len(non_wall_indices) > 0:
            clustering = DBSCAN(eps=self.eps, min_samples=self.min_samples).fit(measurements[non_wall_indices, :2])
            cluster_labels[non_wall_indices] = np.array(clustering.labels_)

        # add -1 as label for walls
        cluster_labels[wall_indices] = -1
        clustered_measurements = np.hstack([measurements, cluster_labels.reshape((-1, 1))])
        return clustered_measurements

    def filter_partitions(self, measurements: np.ndarray) -> np.ndarray:
        """
        Filter labeled measurements, removing clutter and unwanted clusters.

        Unwanted clusters are checked according to the corresponding function of this class instance.

        :param measurements: Nx4 Array of labeled measurements
        :return: Mx4 array of labeled measurements (M<=N) after removing clutter and unwanted clusters
        """
        # TODO handle empty measurements
        # init list of unwanted_cluster_ids, starting with clutter (-1)
        unwanted_cluster_ids = [-1]

        # check all clusters
        for cluster_id_to_check in range(0, int(max(measurements[:, 3]))):
            if self.check_if_cluster_id_is_unwanted(measurements, cluster_id_to_check):
                unwanted_cluster_ids.append(cluster_id_to_check)

        # return only measurements with column 3 (=cluster_id) not in unwanted_cluster_ids
        return measurements[
            [measurements[i, 3] not in unwanted_cluster_ids
             for i in range(len(measurements))]
        ]

    def check_if_cluster_id_is_unwanted(self, measurements: np.ndarray, cluster_id_to_check: int) -> bool:
        """
        Return True if the given cluster ID is "unwanted" in the measurements, i.e., if it does not correspond to a
        cluster that should be used by downstream processing.
        :param measurements: Nx4 measurements
        :param cluster_id_to_check: int of cluster ID to check
        :return: True, if cluster_id_to_check should be filtered from the measurements
        """
        # bool that will be returned
        is_unwanted = False

        # reduce measurements to points corresponding to cluster
        cluster_points = measurements[measurements[:, 3] == cluster_id_to_check]

        # negative cluster_id is clutter:
        if cluster_id_to_check < 0:
            is_unwanted = True

        # empty clusters are useless:
        if len(cluster_points) == 0:
            is_unwanted = True

        # large clusters are most likely walls
        if len(cluster_points) > self.max_samples:
            is_unwanted = True

        # too long or wide clusters are unlikely to be objects
        extent_x = np.max(cluster_points[:, 0]) - np.min(cluster_points[:, 0])
        extent_y = np.max(cluster_points[:, 1]) - np.min(cluster_points[:, 1])
        if extent_x > self.max_object_size or extent_y > self.max_object_size:
            is_unwanted = True

        return is_unwanted

    def filter_walls(self, measurements: np.ndarray):
        """
        Uses Hough transform to identify points belonging to lines and returns indices of those points.
        :param measurements:    measurements as Nx3 array with the first 2 dimension corresponding to the position
        :return:                indices of points belonging to walls and indices of the other points
        """
        is_wall = np.zeros(len(measurements), bool)
        # for i in range(100):
        #     if all(is_wall):
        #         break
        #     angle = i * 2.0*np.pi * 0.01
        #     ranges = measurements[:, 0]*np.cos(angle) + measurements[:, 1]*np.sin(angle)
        #     for ind in np.arange(len(measurements)):
        #         if is_wall[ind]:
        #             continue
        #         distance_to_walls = abs(ranges[ind] - ranges)
        #         wall_point_mask = distance_to_walls < self.wall_precision
        #         if np.sum(wall_point_mask) > self.min_pts_line:
        #             if np.max(cdist(measurements[wall_point_mask, :2], measurements[wall_point_mask, :2])) \
        #                     >= self.min_length_line:
        #                 is_wall[wall_point_mask] = True

        return np.arange(len(measurements))[is_wall], np.arange(len(measurements))[np.invert(is_wall)]
