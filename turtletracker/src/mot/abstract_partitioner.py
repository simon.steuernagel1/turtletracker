from abc import ABC, abstractmethod

import numpy as np


class AbstractPartitioner(ABC):
    """Abstract base class for partionioning of point clouds"""
    @abstractmethod
    def create_partitions(self, measurements: np.ndarray) -> np.ndarray:
        """
        Create a partition from the measurements, which are an Nx3 array (x, y, intensity)
        Returns a point cloud labeling each point with a cluster ID, Nx4 (x, y, intensity, cluster_ID), ID -1 labels
        clutter points
        """
        pass

    @abstractmethod
    def filter_partitions(self, measurements: np.ndarray) -> np.ndarray:
        """
        Remove partitions based on predefined criteria from the partitioned measurements and remove clutter points,
         Nx4 array (x, y, intensity, cluster_ID), ID -1 labels clutter points
        Returns a point cloud labeling each point with a cluster ID, Mx4 (x, y, intensity, cluster_ID), with M<=N
        """
        pass
