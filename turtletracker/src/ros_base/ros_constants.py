"""
This file contains constants which can be used for ROS-related tasks.
"""
# objects with a fixed (unspecified) height should be assigned this value:
FIXED_OBJECT_HEIGHT = 0.1

# object with a fixed (unspecified) z-coordinate should be assigned this value:
FIXED_OBJECT_Z = -0.5

# minimum marker size
MIN_MARKER_SIZE = 0.05
