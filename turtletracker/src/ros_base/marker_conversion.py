"""
This file contains functions to convert from and to ROS marker representations.
"""
import numpy as np
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import Pose, Vector3
from std_msgs.msg import ColorRGBA, Header
from pyquaternion import Quaternion
from turtletracker.src.ros_base.ros_constants import FIXED_OBJECT_HEIGHT, FIXED_OBJECT_Z, MIN_MARKER_SIZE


def objects_to_markers(object_list: list,
                       header: Header,
                       namespace: str,
                       color: list) -> MarkerArray:
    """
    Given a list of objects as list of tuples (uid, state, cov), returns a corresponding ROS MarkerArray
    :param object_list: list of objects to convert
    :param header: Header to be set for all markers
    :param namespace: Namespace to be set for all markers
    :param color: 4D array representing [R, G, B, A] for all markers
    :return: MarkerArray representing the given objects
    """
    marker_array_msg = MarkerArray()
    marker_array_msg.markers = [convert_single_object_to_marker(next_object, header, namespace, color)
                                for next_object in object_list]
    return marker_array_msg


def convert_single_object_to_marker(next_object: tuple,
                                    header: Header,
                                    namespace: str,
                                    color: list) -> Marker:
    """
    Given a single object as tuple (uid, state, covariance), return a Marker for this object
    :param next_object: (uid, state, cov) of object
    :param header: Header to be set for this object
    :param namespace: Namespace to be set for this object
    :param color: 4D array representing [R, G, B, A] for this marker
    :return: Marker corresponding to the target
    """
    # extract object
    uid, state, cov = next_object
    print("uid", uid, "--state", np.around(state,2))  # TODO remove debug print

    # convert to ROS msg type
    color_ros = ColorRGBA()
    color_ros.r, color_ros.g, color_ros.b, color_ros.a = np.array(color).astype(float)

    pose = Pose()
    # extract position, set z to fixed value
    pose.position.x, pose.position.y, pose.position.z = state[0], state[1], FIXED_OBJECT_Z
    # extract rotation, create corresponding quaternion by rotating around z-Axis (i.e., get yaw)
    rot_quaternion = Quaternion(axis=[0, 0, 1], radians=state[4])
    pose.orientation.w, pose.orientation.x, pose.orientation.y, pose.orientation.z, = rot_quaternion.elements

    # extract size and set fixed height to fixed value
    scale = Vector3()
    scale.x, scale.y, scale.z = max(state[5]*2, MIN_MARKER_SIZE), max(state[6]*2, MIN_MARKER_SIZE), FIXED_OBJECT_HEIGHT

    # build Markers based on previous information
    marker_msg = Marker()
    marker_msg.header = header
    marker_msg.ns = namespace
    marker_msg.id = uid
    marker_msg.type = marker_msg.SPHERE
    marker_msg.action = 0  # 0 -> add/modify an object
    marker_msg.pose = pose
    marker_msg.scale = scale
    marker_msg.color = color_ros
    # set duration to 0.1 sec (converted to nanosec and casted to int)
    marker_msg.lifetime.nanosec = int(1e9 * 0.1)
    marker_msg.frame_locked = False
    return marker_msg
