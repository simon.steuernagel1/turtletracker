"""
This file implements a wrapper around a pythonic MOT architecture.
"""
import numpy as np
from turtletracker.src.mot.baseline_mot import BaselineMOT
from turtletracker.src.utils.config_parsing import load_baseline_mot_config
from turtletracker.src.vis.abstract_visualizer import AbstractVisualizer


class BaselineMOTWrapper:
    """
    Simple class that loads a baseline MOT architecture and passes scans to it, returning confirmed and unconfirmed
    results.
    """
    def __init__(self, path_to_config: str, verbose=True, visualizer_instance: AbstractVisualizer = None):
        """
        Create a new instance of the MOTWrapper
        :param path_to_config: Path to config to load for BaselineMOT as str
        :param verbose: bool: whether to be verbose on the console
        :param visualizer_instance: None or an AbstractVisualizer instance to which data is plotted after every time
        step. If None, will be ignored
        """
        self.verbose = verbose
        if self.verbose:
            print("Verbose Ellipse Tracker created. Hello!")

        self.time = None

        self.path_to_config = path_to_config
        self.params = load_baseline_mot_config(self.path_to_config)

        self.mot = BaselineMOT(**self.params)

        self.visualizer_instance = visualizer_instance

    def update(self, scan: np.ndarray, time: float) -> tuple:
        """
        Update the internal MOT with a new scan.

        :param scan: Nx3 array of scan points
        :param time: Time at which the scan was taken
        :return: (confirmed objects, unconfirmed objects) as lists of tuples
        """
        if self.time is None:
            if self.verbose:
                print(f"Starting at time {time}")
            self.time = time

        if time < self.time:
            if self.verbose:
                print(f"Discarding scan @t={time} due to being older than internal time {self.time}")
            return [], []
        time_diff = time - self.time

        # predict if possible
        if time_diff > 0:
            self.mot.predict(time_diff)

        # update
        self.mot.update(scan)

        self.time = time

        # get relevant objects
        confirmed_objects = self.mot.get_confirmed_objects()
        unconfirmed_objects = self.mot.get_unconfirmed_objects()
        last_partition = self.mot.get_last_partition()
        last_filtered_partition = self.mot.get_last_filtered_partition()
        if self.verbose:
            print(f"MOT Wrapper Stats:")
            print(f"\tConfirmed Objects  : {len(confirmed_objects)}")
            print(f"\tUnconfirmed Objects: {len(unconfirmed_objects)}")
            print(f"\tLast Partition consisted of #measurements: {len(last_partition)}")
            print(f"\tAfter filtering, #measurements remained  : {len(last_filtered_partition)}")

        if self.visualizer_instance is not None:
            self.visualizer_instance.plot_scene(confirmed_objects, unconfirmed_objects,
                                                last_partition, last_filtered_partition)

        return confirmed_objects, unconfirmed_objects
