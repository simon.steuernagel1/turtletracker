import numpy as np
from matplotlib.axes import Axes
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import itertools
from collections import deque

from turtletracker.src.vis.abstract_visualizer import AbstractVisualizer


class MultiWindowVisualizer(AbstractVisualizer):
    N_AXES_ROWS = 2
    N_AXES_COLS = 2
    """
    Visualizer that uses a 2x2 Window Structure for more detailed analysis.
    
    The axis (top-left to bottom right) contain the following information:
    - Measurement Scan - colored by intensity
    - Measurement Scan - colored by
    - ...
    - Confirmed and fully filtered objects with trajectories
    
    """

    def __init__(self, live_plot=True):
        self.live_plot = live_plot
        self.pause_interval = 0.1

        if self.live_plot:
            self.fig, self.axs = plt.subplots(self.N_AXES_ROWS, self.N_AXES_COLS)
        else:
            self.fig, self.axs = None, None

        self.params = dict(
            confirmed_alpha=0.7,
            unconfirmed_alpha=0.4
        )

        self.axis_limits = [-5, 5]

        self.last_object_locations = {}

        self.deque_maxlen = 6

        self.robot_location = deque(maxlen=self.deque_maxlen)

    def plot_object(self, object: np.ndarray, object_id: int, ax: Axes, confirmed=True):
        """
        Plots extended object as ellipse
        :param object:      7D np.ndarray with x, y, vx, vy, orientation (rad), semi-axis length, semi-axis width
        :param object_id:   ID of the object as an integer
        :param ax:          axis onto which to plot
        :param confirmed:   boolean, confirmed object get higher alpha value than unconfirmed ones
        """
        ax.scatter(object[0], object[1], marker='x', c='black')
        ellipse = Ellipse(object[:2], object[5] * 2.0, object[6] * 2.0, np.rad2deg(object[4]), fill=True)
        ellipse.set_facecolor('grey')
        ellipse.set_alpha(self.params["confirmed_alpha"]) if confirmed \
            else ellipse.set_alpha(self.params["unconfirmed_alpha"])
        ax.add_patch(ellipse)
        ax.annotate('%i' % object_id, object[:2])

    def plot_object_trajectory(self, object_id: int, ax: Axes, confirmed=True):
        """
        Plots object trajectory as line plot
        :param object_id:   ID of the object as an integer
        :param ax:          axis onto which to plot
        :param confirmed:   boolean, confirmed object get higher alpha value than unconfirmed ones
        """
        state_list = np.array(self.last_object_locations[object_id]).reshape((-1, 2))
        alpha = self.params["confirmed_alpha"] if confirmed else self.params["unconfirmed_alpha"]
        ax.plot(*state_list.T, alpha=alpha, marker='x', linestyle='--')

    def plot_scene(self, confirmed_objects: list, unconfirmed_objects: list, last_partition: np.ndarray,
                   last_filtered_partition: np.ndarray):
        """
        Plot the current scene
        :param confirmed_objects:       list of (UID: int, state: 7D np.ndarray, covariance: 7x7 ndarray) tuples
        :param unconfirmed_objects:     list of (UID: int, state: 7D np.ndarray, covariance: 7x7 ndarray) tuples
        :param last_partition:          np.ndarray, Nx4 (x, y, intensity, cluster_ID) with clutter marked with ID -1
        :param last_filtered_partition: np.ndarray, Nx4 (x, y, intensity, cluster_ID)
        """
        if not self.live_plot:
            fig, axs = plt.subplots(self.N_AXES_ROWS, self.N_AXES_COLS)
        else:
            fig, axs = self.fig, self.axs
            for ax in axs.flatten():
                plt.sca(ax)
                plt.cla()

        # put parameter into a single dict for kwargs call later:
        collected_parameters = {
            "confirmed_objects": confirmed_objects,
            "unconfirmed_objects": unconfirmed_objects,
            "last_partition": last_partition,
            "last_filtered_partition": last_filtered_partition
        }

        self.update_internal_trajectories(**collected_parameters)

        # === START VISUALS

        # ===========
        # 1. TOP LEFT
        self.plot_intensity_scan(ax=axs[0, 0], **collected_parameters)
        self.plot_robot_location(ax=axs[0, 0])

        # ============
        # 2. TOP RIGHT
        self.plot_clustered_scan(ax=axs[0, 1], **collected_parameters)
        self.plot_robot_location(ax=axs[0, 1])

        # ==============
        # 3. BOTTOM LEFT
        self.plot_object_centers(ax=axs[1, 0], **collected_parameters)
        self.plot_robot_location(ax=axs[1, 0])

        # ===============
        # 4. BOTTOM RIGHT
        self.plot_confirmed_object_centers(ax=axs[1, 1], **collected_parameters)
        self.plot_robot_location(ax=axs[1, 1])

        # === END VISUALS
        for ax in axs.flatten():
            plt.sca(ax)
            plt.xlim(self.axis_limits)
            plt.ylim(self.axis_limits)
            plt.gca().set_aspect('equal', adjustable='box')
        if self.live_plot:
            plt.draw()
            plt.pause(self.pause_interval)
        else:
            plt.show()

    def plot_intensity_scan(self,
                            ax,
                            confirmed_objects: list,
                            unconfirmed_objects: list,
                            last_partition: np.ndarray,
                            last_filtered_partition: np.ndarray):
        ax.scatter(last_partition[:, 0], last_partition[:, 1], marker='.', c=last_partition[:, 2])
        ax.set_title("Scan (Intensity)")

    def plot_clustered_scan(self,
                            ax,
                            confirmed_objects: list,
                            unconfirmed_objects: list,
                            last_partition: np.ndarray,
                            last_filtered_partition: np.ndarray):
        ax.scatter(last_partition[:, 0], last_partition[:, 1], marker='.', c=last_partition[:, 3])
        ax.set_title("Scan (Clustered)")

    def plot_object_centers(self,
                            ax,
                            confirmed_objects: list,
                            unconfirmed_objects: list,
                            last_partition: np.ndarray,
                            last_filtered_partition: np.ndarray):
        # plot object centers
        for i in range(len(confirmed_objects)):
            self.plot_object(confirmed_objects[i][1], confirmed_objects[i][0], ax, True)
        for i in range(len(unconfirmed_objects)):
            self.plot_object(unconfirmed_objects[i][1], unconfirmed_objects[i][0], ax, False)
        ax.set_title("Object Centers")

    def plot_confirmed_object_centers(self,
                                      ax,
                                      confirmed_objects: list,
                                      unconfirmed_objects: list,
                                      last_partition: np.ndarray,
                                      last_filtered_partition: np.ndarray):
        # plot object centers
        for i in range(len(confirmed_objects)):
            self.plot_object(confirmed_objects[i][1], confirmed_objects[i][0], ax, True)
        ax.set_title("Confirmed Object Centers")

    def update_internal_trajectories(self,
                                     confirmed_objects: list,
                                     unconfirmed_objects: list,
                                     last_partition: np.ndarray,
                                     last_filtered_partition: np.ndarray):
        # pop 1 thing per internal trajectory list
        ids_to_pop = []
        for obj_id in self.last_object_locations:
            try:
                self.last_object_locations[obj_id].popleft()
            except IndexError:
                # was already empty, remove to save memory
                ids_to_pop.append(obj_id)
        for obj_id in ids_to_pop:
            self.last_object_locations.pop(obj_id)

        obj_id_list = list(self.last_object_locations.keys())

        # update the internal trajectory list:
        for obj_state in itertools.chain(confirmed_objects, unconfirmed_objects):
            obj_id, state, cov = obj_state

            # create new entry for new object id
            if obj_id not in obj_id_list:
                self.last_object_locations[obj_id] = deque(maxlen=self.deque_maxlen)

            self.last_object_locations[obj_id].append(state[:2])

    def plot_objects_with_trajectories(self, ax,
                                       confirmed_objects: list,
                                       unconfirmed_objects: list,
                                       last_partition: np.ndarray,
                                       last_filtered_partition: np.ndarray):
        # plot object centers
        for i in range(len(confirmed_objects)):
            self.plot_object_trajectory(confirmed_objects[i][0], ax, True)
            self.plot_object(confirmed_objects[i][1], confirmed_objects[i][0], ax, True)
        for i in range(len(unconfirmed_objects)):
            self.plot_object(unconfirmed_objects[i][1], unconfirmed_objects[i][0], ax, False)
        ax.set_title("Object Trajectories")

    def plot_robot_location(self, ax):
        """Plot the robot location on an axis"""
        pts = np.array([x[:2] for x in self.robot_location]).reshape((-1, 2))
        ax.plot(pts[:, 0], pts[:, 1], marker='x', linestyle='--', c='red')

    def update_robot_location(self, location: np.ndarray):
        self.robot_location.append(location)
