import numpy as np
from matplotlib.axes import Axes
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse

from turtletracker.src.vis.abstract_visualizer import AbstractVisualizer


class BasicVisualizer(AbstractVisualizer):
    """
    Simple visualizer for plotting the scenes each time step.
    """

    def __init__(self, live_plot=True):
        self.live_plot = live_plot
        self.pause_interval = 0.1

        self.robot_location = None

        if self.live_plot:
            self.fig, self.ax = plt.subplots(1, 1)
        else:
            self.fig, self.ax = None, None

        self.axis_limits = [-5, 5]

    def plot_object(self, object: np.ndarray, object_id: int, ax: Axes, confirmed=True):
        """
        Plots extended object as ellipse-
        :param object:      7D np.ndarray with x, y, vx, vy, orientation (rad), semi-axis length, semi-axis width
        :param object_id:   ID of the object as an integer
        :param ax:          axis onto which to plot
        :param confirmed:   boolean, confirmed object get higher alpha value than unconfirmed ones
        """
        ax.scatter(object[0], object[1], marker='x', c='black')
        ellipse = Ellipse(object[:2], object[5] * 2.0, object[6] * 2.0, np.rad2deg(object[4]), fill=True)
        ellipse.set_facecolor('grey')
        ellipse.set_alpha(0.7) if confirmed else ellipse.set_alpha(0.4)
        ax.add_patch(ellipse)
        ax.annotate('%i' % object_id, object[:2])

    def plot_scene(self, confirmed_objects: list, unconfirmed_objects: list, last_partition: np.ndarray,
                   last_filtered_partition: np.ndarray):
        """

        :param confirmed_objects:       list of (UID: int, state: 7D np.ndarray, covariance: 7x7 ndarray) tuples
        :param unconfirmed_objects:     list of (UID: int, state: 7D np.ndarray, covariance: 7x7 ndarray) tuples
        :param last_partition:          np.ndarray, Nx4 (x, y, intensity, cluster_ID) with clutter marked with ID -1
        :param last_filtered_partition: np.ndarray, Nx4 (x, y, intensity, cluster_ID)
        """
        if not self.live_plot:
            fig, ax = plt.subplots(1, 1)
        else:
            fig, ax = self.fig, self.ax
            plt.cla()

        # plot object centers
        for i in range(len(confirmed_objects)):
            self.plot_object(confirmed_objects[i][1], confirmed_objects[i][0], ax, True)
        for i in range(len(unconfirmed_objects)):
            self.plot_object(unconfirmed_objects[i][1], unconfirmed_objects[i][0], ax, False)

        # plot measurements
        ax.scatter(last_partition[:, 0], last_partition[:, 1], marker='.', c=last_partition[:, 3])

        # plot own location
        if self.robot_location is not None:
            ax.scatter(*self.robot_location[:2], marker='x', c='r')

        plt.xlim(self.axis_limits)
        plt.ylim(self.axis_limits)
        plt.gca().set_aspect('equal', adjustable='box')
        if self.live_plot:
            plt.draw()
            plt.pause(self.pause_interval)
            plt.draw()
        else:
            plt.show()

    def update_robot_location(self, location: np.ndarray):
        self.robot_location = location
