from abc import ABC, abstractmethod

import numpy as np


class AbstractVisualizer(ABC):
    """Abstract base class for visualizing of tracker results"""

    @abstractmethod
    def plot_scene(self, confirmed_objects: list, unconfirmed_objects: list, last_partition: np.ndarray,
                   last_filtered_partition: np.ndarray):
        """
        Create a plot for the current scene given all processed data from the current time step.
        """

    def update_robot_location(self, location: np.ndarray):
        """
        Pass a new robot location to the visualizer. By default, nothing needs to be done with this, but it can be
        integrated into the robot trajectory.
        """
        pass
