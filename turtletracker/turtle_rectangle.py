import numpy as np
import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist
from turtlebot3_msgs.msg import Sound


class TurtleController(Node):

    def __init__(self, topic):
        super().__init__('turtle_controller')
        self.topic = topic
        #self.qos_policy = rclpy.qos.QoSProfile(reliability=rclpy.qos.ReliabilityPolicy.RELIABLE,
        #                                       history=rclpy.qos.HistoryPolicy.KEEP_LAST, depth=1)
        #self.publisher_ = self.create_publisher(Twist, self.topic, qos_profile=self.qos_policy)
        self.publisher_ = self.create_publisher(Twist, self.topic, 0)
        self.sound_pub = self.create_publisher(Sound, "/sound", 0)
        timer_period = 1  # seconds
        self.timer = self.create_timer(timer_period, self.mvmnt_timer_callback)
        self.i = 0

    def mvmnt_timer_callback(self):
        msg = Twist()

        max_steps = 40

        if self.i < max_steps:
            if (self.i % 10) < 5:
                forward_speed = 0.1
                msg.linear.x = float(forward_speed)
                yaw_rate = 0
                msg.angular.z = float(yaw_rate)
            else:
                forward_speed = 0.0
                msg.linear.x = float(forward_speed)
                yaw_rate = 0.1*np.pi
                msg.angular.z = float(yaw_rate)

            # publish twist msg
            self.publisher_.publish(msg)
            self.i += 1

            print(f"Finished sending msg {self.i+1} with {forward_speed = } and {yaw_rate = }")

        elif True:#self.i <= max_steps+10:
            msg.angular.z = float(0.0)
            self.publisher_.publish(msg)
            self.i += 1

            s = Sound()
            s.value = 2
            self.sound_pub.publish(s)
            print("published sound")

def main(args=None):
    print("Running turtle_driving!")
    rclpy.init(args=args)

    controller = TurtleController(topic='/cmd_vel')

    rclpy.spin(controller)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    controller.destroy_node()
    rclpy.shutdown()
