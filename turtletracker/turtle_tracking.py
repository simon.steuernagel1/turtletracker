import numpy as np
import rclpy
from rclpy.node import Node

from sensor_msgs.msg import PointCloud2, LaserScan
from std_msgs.msg import Header
from tf2_msgs.msg import TFMessage
from visualization_msgs.msg import Marker, MarkerArray
# note that point_cloud2.py is not in sensor_msgs as of ROS2!
import sensor_msgs_py.point_cloud2 as pc2
from turtletracker.src.ros_base import laser_geometry as lg

from turtletracker.src.ros_base.mot_to_ros_connector import BaselineMOTWrapper
from turtletracker.src.ros_base.marker_conversion import objects_to_markers
from turtletracker.src.vis.basic_visualizer import BasicVisualizer
from turtletracker.src.vis.multi_window_visualizer import MultiWindowVisualizer

from tf2_ros import TransformException
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener

from pyquaternion import Quaternion


def transform_point_cloud(points, R, t):
    points[:, :3] = points[:, :3]@R.T + t[None, :]

    return points


class TurtleTrackingNode(Node):
    """
    Node that subscribes to a LaserScan topic, processes the incoming data, and publishes Markers for confirmed and
    unconfirmed tracked objects.
    """

    def __init__(self, path_to_config, laser_topic_id, marker_topic_id, unconf_marker_topic_id, visualizer):
        super().__init__("scan_subscriber")
        self.laser_topic_id = laser_topic_id

        # prepare transformation
        self.declare_parameter('target_frame', 'base_scan')
        self.target_frame = self.get_parameter(
            'target_frame').get_parameter_value().string_value
        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer, self)

        # create subscriber for laser scan
        # need to define a QoS policy for the subscription to the LaserScan topic
        self.qos_policy = rclpy.qos.QoSProfile(reliability=rclpy.qos.ReliabilityPolicy.BEST_EFFORT,
                                               history=rclpy.qos.HistoryPolicy.KEEP_LAST, depth=1)
        self.subscription = self.create_subscription(LaserScan, self.laser_topic_id, self.callback,
                                                     qos_profile=self.qos_policy)
        self.subscription_tf = self.create_subscription(TFMessage, '/tf', self.tf_callback,
                                                        qos_profile=self.qos_policy)
        print(f"Listening on {self.laser_topic_id}")

        # create publishers for Markers
        self.target_publisher = self.create_publisher(
            MarkerArray,
            marker_topic_id,
            10
        )
        self.unconf_target_publisher = self.create_publisher(
            MarkerArray,
            unconf_marker_topic_id,
            10
        )

        # init necessary objects for later use
        self.lp = lg.LaserProjection()
        self.tracker = BaselineMOTWrapper(path_to_config=path_to_config, visualizer_instance=visualizer)

        # set up a few values as needed
        self.marker_namespace = "turtletracking/"
        self.confirmed_color = [0, 0, 1, 1]  # blue
        self.unconfirmed_color = [1, 0, 0, 1]  # red

        self.R = np.eye(3)
        self.t = np.zeros(3)
        self.last_trans = None

    def tf_callback(self, msg: TFMessage):
        trans = msg.transforms[0]
        if (trans.header.frame_id == 'odom') & (trans.child_frame_id == 'base_footprint'):
            self.R = Quaternion(a=trans.transform.rotation.w, b=trans.transform.rotation.x,
                                c=trans.transform.rotation.y, d=trans.transform.rotation.z).rotation_matrix
            self.t = np.array([trans.transform.translation.x, trans.transform.translation.y,
                               trans.transform.translation.z - 0.1])  # -0.1 from scanner to base
            self.last_trans = trans.header.stamp

        if self.tracker.visualizer_instance is not None:
            self.tracker.visualizer_instance.update_robot_location(self.t)

    def callback(self, msg: LaserScan):
        """
        Process a single laser scan, updating the internal tracker and performing a visualization step.
        :param msg: Incoming LaserScan
        """
        # project laser scan to PointCloud2 format
        pc2_msg = self.lp.projectLaser(msg)

        if self.last_trans is not None and (pc2_msg.header.stamp.sec + pc2_msg.header.stamp.nanosec*1e-9) \
                > (self.last_trans.sec + self.last_trans.nanosec*1e-9) + 0.05:
            print("Skip measurement step due to missing transform")
            return

        # TODO transform pc2msg to odom frame before processing! ros2 run tf2_tools view_frames
        # try:
        #     trans = self.tf_buffer.lookup_transform('odom', 'base_footprint', pc2_msg.header.stamp,
        #                                             timeout=rclpy.duration.Duration(seconds=0.5))
        # except Exception as e:
        #     print(str(e))
        #     trans = None
        #
        # if trans is not None:
        #     R = Quaternion(a=trans.transform.rotation.w, b=trans.transform.rotation.x, c=trans.transform.rotation.y,
        #                    d=trans.transform.rotation.z).rotation_matrix
        #     t = np.array([trans.transform.translation.x, trans.transform.translation.y, trans.transform.translation.z-0.1])
        # else:
        #     R = np.eye(3)
        #     t = np.zeros(3)

        self.frame = pc2_msg.header.frame_id
        # read points
        point_generator = pc2.read_points(pc2_msg)
        # contains x - y - z - intensity - index
        cloud = transform_point_cloud(np.array([p for p in point_generator])[:, [0, 1, 2, 3]], self.R,
                                      self.t)[:, [0, 1, 3]]
        #print(np.min(cloud[:, 2]), np.max(cloud[:, 2]))
        #cloud = cloud[(cloud[:, 2] > 140.0) & (cloud[:, 2] < 230.0)]
        # convert current time to float
        time = msg.header.stamp.sec + (msg.header.stamp.nanosec / 1e9)

        # pass to tracker
        conf_obj, unconf_obj = self.tracker.update(cloud, time)

        # visualize results
        self.visualize_objects(conf_obj, unconf_obj)

    def get_time_stamp(self):
        """
        Return the current time
        :return: ROS time stamp right now
        """
        return self.get_clock().now().to_msg()

    def visualize_objects(self, conf_obj, unconf_obj):
        """
        Visualize objects from a single time step by publishing on the corresponding topics.

        :param conf_obj: Confirmed objects
        :param unconf_obj: Unconfirmed objects
        """
        header = Header()
        header.frame_id = self.frame
        stamp = self.get_time_stamp()
        stamp.nanosec = 0  # TODO overwriting nanosec to 0 to prevent tf needing to project in future
        header.stamp = stamp
        conf_markers = objects_to_markers(conf_obj, header=header,
                                          namespace=self.marker_namespace, color=self.confirmed_color)
        unconf_markers = objects_to_markers(unconf_obj, header=header,
                                            namespace=self.marker_namespace, color=self.unconfirmed_color)
        self.target_publisher.publish(conf_markers)
        self.unconf_target_publisher.publish(unconf_markers)


def main(args=None):
    print('Hi from df_lab scanner.')

    rclpy.init(args=args)

    # TODO there is probably a better way to do this in ROS which I dont know about
    path_to_config = "install/turtletracker/share/turtletracker/configs/baseline_mot/default.json"

    # define matplotlib-based visualizer called after each scan was processed
    visualizer = ...

    # overwrite visualizer with None in order to disable internal visualization
    # visualizer = BasicVisualizer()
    visualizer = MultiWindowVisualizer()

    subscriber = TurtleTrackingNode(
        path_to_config,
        "/scan",
        "/tt/confirmed_objects",
        "/tt/unconfirmed_objects",
        visualizer=visualizer
    )

    try:
        rclpy.spin(subscriber)
    except BaseException as err:
        print(f"Unexpected {err=}, {type(err)=}")
    print("no more spinning...")
    subscriber.destroy_node()
    rclpy.shutdown()

    print("Goodby from df_lab")


if __name__ == '__main__':
    main()
