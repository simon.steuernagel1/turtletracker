# TODOs

- Improve `BasicVisualizer`
  - unordered list! 
  - [ ] Show associations in plot
  - [ ] Filtered out clusters should be visualized with (much) lower alpha
  - [X] Multiple subplots
    - maybe not as part of BasicVisualizer but as a new one instead
    - different processing steps as individual subplots
  - [ ] Plot velocity as arrows
  - [X] Plot trajectory of objects (only last X steps and fade out)
- Improve Algorithm
  - [X] Distance cluster to object should take object extent estimate (and uncertainty) into account 
    - maybe just add both and use for mahalanobis
  - [ ] Improve clustering filtering
    - [ ] ensure walls are filtered out
      - [X] add line filtering as preprocessing before clustering
      - [ ] consider gaps in line filtering to avoid removal of small objects on the same line as a wall
  - [X] Motion Model tuning etc (@kolja)
- Improve Parameterization
  - [ ] re-check DBSCAN parameters, esp. `minpts`
- Transformation in world coordinates
  - [X] transform laser scans into global coordinates
- Handle ID switches
  - [ ] Make sure stationary and well-observed objects are tracked under one ID
