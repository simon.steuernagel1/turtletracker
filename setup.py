from setuptools import setup
from glob import glob
import os

package_name = 'turtletracker'

setup(
    name=package_name,
    version='0.0.0',
    packages=[
        package_name,
        f'{package_name}.src.eot',
        f'{package_name}.src.mot',
        f'{package_name}.src.ros_base',
        f'{package_name}.src.utils',
        f'{package_name}.src.vis'
    ],
    data_files=[
        ('share/ament_index/resource_index/packages', ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (f'share/{package_name}/configs/baseline_mot', ["configs/baseline_mot/default.json"])
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='df-lab',
    maintainer_email='df-lab@todo.todo',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'tracking = turtletracker.turtle_tracking:main',
            'driving = turtletracker.turtle_driving:main',
            'rectangling = turtletracker.turtle_rectangle:main'
        ],
    },
)
